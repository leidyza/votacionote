package com.example.votacionote.Vista.PrincipalView;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.votacionote.Presentador.MainPresenter.PresentadorPrincipal;
import com.example.votacionote.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class VistaPrincipal extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private PresentadorPrincipal presentadorPrincipal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        presentadorPrincipal = new PresentadorPrincipal(this, mDatabase, mAuth);
        presentadorPrincipal.mensajeBienvenida();

    }


}