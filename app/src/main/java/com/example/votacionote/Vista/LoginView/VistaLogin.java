package com.example.votacionote.Vista.LoginView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.votacionote.Presentador.LoginPresenter.PresentadorLogin;
import com.example.votacionote.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class VistaLogin extends AppCompatActivity implements View.OnClickListener {

    private EditText etEmail, etPassword;
    private PresentadorLogin presentadorLogin;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        presentadorLogin = new PresentadorLogin(this, mAuth, mDatabase);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnsignin1);
        btnLogin.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnsignin1:
                String email = etEmail.getText().toString().trim();
                String pass = etPassword.getText().toString().trim();
                presentadorLogin.signInUser(email, pass);
                break;
        }
    }
}