package com.example.votacionote.Vista.RegistroView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.votacionote.Presentador.RegisterPresenter.PresentadorRegister;
import com.example.votacionote.R;
import com.example.votacionote.Vista.LoginView.VistaLogin;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class VistaRegistro extends AppCompatActivity implements View.OnClickListener {

    private EditText etNombre, etUsarname, etEmail, etPass, etRePass;
    private PresentadorRegister presentadorRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        presentadorRegister = new PresentadorRegister(this, mAuth, mDatabase);
        etNombre = findViewById(R.id.etNombre);
        etUsarname = findViewById(R.id.etUserName);
        etEmail = findViewById(R.id.etEmail);
        etPass = findViewById(R.id.etPassword);
        etRePass = findViewById(R.id.etRePassword);
        Button btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);

        //Para direccionar al login
        Button btnLogin = findViewById(R.id.btnsignin);
        btnLogin.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRegister:
                String nombre = etNombre.getText().toString().trim();
                String usuario = etUsarname.getText().toString().trim();
                String email = etEmail.getText().toString().trim();
                String pass = etPass.getText().toString().trim();
                String repass = etRePass.getText().toString().trim();
                if(pass.equals(repass)){
                    presentadorRegister.signUpUser(email, usuario, nombre, pass);
                }else{
                    Toast.makeText(this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.btnsignin:
                Intent intent = new Intent(VistaRegistro.this, VistaLogin.class);
                startActivity(intent);

        }
    }
}