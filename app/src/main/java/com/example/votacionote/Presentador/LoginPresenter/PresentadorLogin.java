package com.example.votacionote.Presentador.LoginPresenter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.votacionote.Vista.PrincipalView.VistaPrincipal;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

public class PresentadorLogin {
    private static final String TAG = "PresentadorLogin";
    private Context mContext;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    public PresentadorLogin(Context mContext, FirebaseAuth mAuth, DatabaseReference mDatabase) {
        this.mContext = mContext;
        this.mAuth = mAuth;
        this.mDatabase = mDatabase;
    }

    public PresentadorLogin(Context mContext) {
    }

    public void signInUser(String email, String password) {
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Ingresando...");
        dialog.setCancelable(false);
        dialog.show();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithEmail:Exitoso");
                            dialog.dismiss();
                            mDatabase.child("Usuarios").child(task.getResult().getUser().getUid()).child("titulo").setValue("FirebaseMVP");
                            Intent intent = new Intent(mContext, VistaPrincipal.class);
                            mContext.startActivity(intent);

                        } else {
                            dialog.dismiss();
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(mContext, "Authentication failed.", Toast.LENGTH_SHORT).show();

                        }

                    }
                });

    }
}
