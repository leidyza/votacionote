package com.example.votacionote.Presentador.MainPresenter;

import android.content.Context;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.votacionote.Modelo.UserModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

public class PresentadorPrincipal {
    private Context mContext;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    public PresentadorPrincipal(Context mContext, DatabaseReference mDatabase, FirebaseAuth mAuth) {
        this.mContext = mContext;
        this.mDatabase = mDatabase;
        this.mAuth = mAuth;
    }

    public void mensajeBienvenida(){
        mDatabase.child("Usuarios").child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                UserModel userModel = dataSnapshot.getValue(UserModel.class);
                Toast.makeText(mContext, "Bienvenido "+userModel.getNombre(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}
